import binascii
import random
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def file2bin(input_file, output_bin):
    '''
        Creates a binary file as output from a given input file with no particular format.
            Inputs:
                -input_file: [str] source file to transfer (any extension will work [eg: .jpg, .png, .mp3, .pptx, ...])
                -output_bin: [str] binary file created from the input file codification.

            Outputs:
                -bin_string: [str] binary string with the information of the input file.
    '''

    # Definition of files handler.
    writer = open(output_bin, 'w')

    # reading the contents of the input file 
    with open(input_file, 'rb') as f:
        content = f.read()

    # Changing the contents from the input file format to hex codification.
    bin_string = binascii.hexlify(content)
    #print(bin_string[0:100])
    
    bin_string = bin(int(bin_string, 16))[2:]
    writer.write(f'{bin_string}')
    print(f'[INFO] Hex file successfully created as: {output_bin}')

    # Closing files.
    f.close()
    writer.close()
    
    return bin_string

def bin2file(bin_string, output_file):

    '''
    Converts from an input hex file to a selected format output file. 
        Inputs:
            -bin_string: [str] binary string received from an external source.
            -output_file: [str] result of the re transformation of binary data to a particular file.
    '''

    with open(output_file, 'wb') as fout:
        # Re-transforming the binary string to data
        bin_string = hex( int(bin_string, 2) )[2:]
        fout.write(binascii.unhexlify(bin_string))
        fout.close()

    print(f'[INFO] New file created succesfully. Name: {output_file}')




def extension_handler(input_file):
    '''
        This function gets the extension from the input file and creates the output_file with the same extension
            Inputs:
                -input_file: [str] hardcoded input file from the main function (including its extension)
            Outputs: 
                -output_bin: [str] path and name (hardcoded) to the output binary string to transmit
                -output_file [str] path and name (with the same extension as the input) to the received file.
    '''
    # Splitting the path given as input
    path = os.path.split(input_file)
    output_path = path[0]
    extension = path[1]
    print(extension)
    # Getting the extension of the file given as input
    extension = extension.split('.')
    extension = extension[1]

    # Output paths definition
    output_bin = os.path.join(output_path, 'output_bin.txt')
    output_file = os.path.join(output_path, f'output_file.{extension}')

    return output_bin, output_file

def BSC_str(transmitted_bits, pe):
    transmitted_bits = list(transmitted_bits)
    print(len(transmitted_bits))
    for i in range(len(transmitted_bits)):
        rand = random.random()
        if(rand < pe):
            if transmitted_bits[i] == "1":
                transmitted_bits[i] = "0"
            else:
                transmitted_bits[i] = "1"
    return ''.join(transmitted_bits)

def main():
    input_file = "captura.bmp"

    output_bin, output_file = extension_handler(input_file)

    bin_string = file2bin(input_file, output_bin)

    trans_bin = BSC_str(bin_string, 0.002)

    lista = []

    for i in range(len(trans_bin)):
        if (trans_bin[i] != bin_string[i]):
            lista.append(i)

    
    print(f'[INFO] Error not fixed: {len(lista)}')


    # Transforming the binary string to an output file
    bin2file(trans_bin,output_file)

if __name__ == '__main__':
    main()
    
img = mpimg.imread('output_file.bmp')

imgplot = plt.imshow(img)