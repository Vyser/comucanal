import binascii
import random
import os
import sys
import numpy as np

'''
    Universidad de Costa Rica
    IE-0527: Ingenieria de Comunicaciones
    Simulation Project: Canal

    Project group: Pypas

        Abstract: 

        Requierements:
            1) Python3 for running
            2) Change the hardcoded path in the main function to your desired test files.
            3) Use either Windows format or Linux format to specify the path. Comment the other one.
            
        Notes: 
            -This program needs a hardcoded input from the user to the input file (to transform) 
            -This program creates 2 different outputs: 
                -output_bin.txt: file with the binary codification of the input file
                -output_file.[extension]: file re-transformed to the same extension as the input

        Block diagram of the flow:
     ----------                               --------------                    -----------
    |          |     -------------------     |              |    ---------     |           |
    |input_file|---->Binary codification---->|Binary message|---->Decoding---->|output_file|
    |          |     -------------------     |              |    ---------     |           |
     ----------                               --------------                    -----------
        |                                          |                                 |
        |                                          |                                 |
        ---->from user (in main function)          ---->Saved to output_bin.txt      ---->Saved to output_file
'''


def file2bin(input_file, output_bin):
    '''
        Creates a binary file as output from a given input file with no particular format.
            Inputs:
                -input_file: [str] source file to transfer (any extension will work [eg: .jpg, .png, .mp3, .pptx, ...])
                -output_bin: [str] binary file created from the input file codification.

            Outputs:
                -bin_string: [str] binary string with the information of the input file.
    '''

    # Definition of files handler.
    writer = open(output_bin, 'w')

    # reading the contents of the input file 
    with open(input_file, 'rb') as f:
        content = f.read()

    # Changing the contents from the input file format to hex codification.
    bin_string = binascii.hexlify(content)
    bin_string = bin(int(bin_string, 16))[2:]
    writer.write(f'{bin_string}')
    print(f'[INFO] Hex file successfully created as: {output_bin}')

    # Closing files.
    f.close()
    writer.close()
    
    return bin_string

def bin2file(bin_string, output_file):

    '''
    Converts from an input hex file to a selected format output file. 
        Inputs:
            -bin_string: [str] binary string received from an external source.
            -output_file: [str] result of the re transformation of binary data to a particular file.
    '''

    with open(output_file, 'wb') as fout:
        # Re-transforming the binary string to data
        bin_string = hex( int(bin_string, 2) )[2:]
        fout.write(binascii.unhexlify(bin_string))
        fout.close()

    print(f'[INFO] New file created succesfully. Name: {output_file}')

def extension_handler(input_file):
    '''
        This function gets the extension from the input file and creates the output_file with the same extension
            Inputs:
                -input_file: [str] hardcoded input file from the main function (including its extension)
            Outputs: 
                -output_bin: [str] path and name (hardcoded) to the output binary string to transmit
                -output_file [str] path and name (with the same extension as the input) to the received file.
    '''
    # Splitting the path given as input
    path = os.path.split(input_file)
    output_path = path[0]
    extension = path[1]
    print(extension)
    # Getting the extension of the file given as input
    extension = extension.split('.')
    extension = extension[1]

    # Output paths definition
    output_bin = os.path.join(output_path, 'output_bin.txt')
    output_file = os.path.join(output_path, f'output_file.{extension}')

    return output_bin, output_file

def BSC_str(transmitted_bits, pe):
    transmitted_bits = list(transmitted_bits)
    print(len(transmitted_bits))
    for i in range(len(transmitted_bits)):
        rand = random.random()
        if(rand < pe):
            if transmitted_bits[i] == "1":
                transmitted_bits[i] = "0"
            else:
                transmitted_bits[i] = "1"
    return ''.join(transmitted_bits)


def channelError(syndrome):
    H = np.array([[1, 0, 0],[ 0, 1, 0],[ 0, 0, 1],[ 1, 1, 0],[ 0, 1, 1],[ 1, 0, 1]])
    dim = len(H)
    errorArray = np.zeros(dim)
    counter = 0
    errorLocation = 0
    for row in H:
        comparison = row == syndrome
        if comparison.all():
            errorLocation = counter
        counter = counter+1
    if errorLocation != 0:
        errorArray[errorLocation] = 1
    
    return errorArray

def BSC_v2(transmitted_bits, pe):
    #print(len(transmitted_bits))
    num_errores = 0
    for i in range(len(transmitted_bits)):
        for j in range(len(transmitted_bits[0])):
            rand = random.random()
            if(rand < pe):
                num_errores +=1
                transmitted_bits[i][j] = not(transmitted_bits[i][j])
    print("Cantidad de errores generados: ", num_errores)
    return transmitted_bits

def channelEncoder(string_bits):
    """ Channel encoder """
    #string_in = string_bits[0:103]
    bit_sequence = list(string_bits)
    bit_sequence = [int(bit) for bit in bit_sequence]

    #En encoder_output se tendra el result final cuando se termine la codificacion de todos los conjuntos de k bits
    encoder_output = np.empty([1,6], int)
    #Define la matriz generadora G
    G = np.array([[1, 1, 0, 1, 0, 0], [ 0, 1, 1, 0, 1, 0], [ 1, 0, 1, 0, 0, 1]])
    #Limites para recorrer el arreglo
    k = 3
    n = 0
    m = 3

    #Guarda la cantidad de zeroes agregados en la bit_sequence en caso de que no sea múltiplo de k = 3
    zeroes = 0

    while(len(bit_sequence)%k != 0): #Revisa que no vayan a quedar vectores con menos de k elementos
        bit_sequence.append(0)
        zeroes+=1

    #Recorre todo el arreglo de entrada
    while n < len(bit_sequence):
        #Se obtienen los k = 3 bits para codificar
        encoder_input = np.array([bit_sequence[n:m]])
        multiplicacion = encoder_input@G
            
        for i in multiplicacion[0]:
            multiplicacion[0][i] = multiplicacion[0][i] % 2
        
        # print(multiplicacion[0])

        if n == 0: #Inicizaliza el np.array
            encoder_output= multiplicacion
        else: #apila los demas resultados
            encoder_output = np.vstack([encoder_output, multiplicacion])
        n+=k
        m+=k

    string = np.array_str(encoder_output)
    string = string.replace('[', '')
    string = string.replace(']', '')
    string = string.replace(' ', '')
    string = string.replace('\n', '')

    return encoder_output, zeroes, string_bits

def channelDecoder(encoder_output, string):
    """ Channel decoder """
    H = np.array([[1, 0, 0],[ 0, 1, 0],[ 0, 0, 1],[ 1, 1, 0],[ 0, 1, 1],[ 1, 0, 1]])

    # To go through all the 6 bits received. 6 bits are obtained from the matrix multiplication m*G
    n = 0
    m = 6

    sindromes = np.empty([1,3], int)
    decoder_output = []
    # Recorremos todos los bits codificados para hallar el sindrome con S = vH
    while n < encoder_output.shape[0]:
        multiplicacion_sindromes = np.remainder(encoder_output[n]@H, 2) #Multiplicacion de matrices y modulo 2

        #print(multiplicacion_sindromes)

        if (n==0):
            sindromes = multiplicacion_sindromes 
        else:
            sindromes = np.vstack([sindromes, multiplicacion_sindromes])

        # Simple error correction with syndrome
        for i in range(m): #
            if (H[i] == multiplicacion_sindromes).all():

                if encoder_output[n][i] == 1:
                    encoder_output[n][i] = 0
                else:
                    encoder_output[n][i] = 1

        # Get the last 3 bits which are packaged according to the G matrix
        decoder_output.extend(encoder_output[n][3:6].tolist())
        # get the next 6 bits
        n+=1

    decoder_outputStr = ''.join(str(bit) for bit in decoder_output)
    diff = len(decoder_outputStr) - len(string)
    if diff != 0:
        decoder_outputStr = decoder_outputStr[:-diff]
    
    return decoder_outputStr

def main():
    input_file = sys.argv[1]

    # setting the output files to the same location and extension as the input file
    output_bin, output_file = extension_handler(input_file)

    # Transforming the input file to binary
    bin_string = file2bin(input_file, output_bin)

    trans_bin, zeroes, string = channelEncoder(bin_string)

    trans_bin = BSC_v2(trans_bin, 0.05)

    # print(trans_bin)

    rec_bin = channelDecoder(trans_bin, string)

    lista = []

    for i in range(len(string)):
        if string[i] != rec_bin[i]:
            lista.append(i)
    
    print(f'[INFO] Error not fixed: {len(lista)}')


    #Transforming the binary string to an output file
    bin2file(rec_bin,output_file)

if __name__ == '__main__':
    main()
